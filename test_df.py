#!/usr/bin/python

import re
import json
import urllib2
import time
import sys

df_url = 'http://ocj.live.bestvcdn.com.cn/live/program/live/dfgwsx/live.m3u8?se=ocj&ct=2'
# df_sub_m3u8 = 'http://tt-ocjlive.bestvcdn.com.cn/live/program/Docj.live.bestvcdn.com.cnD/_0i7h004P86_/live/dfgwsx/2500000/mnf.m3u8'
df_m3u8_prefix = 'http://tt-ocjlive.bestvcdn.com.cn/live/program/Docj.live.bestvcdn.com.cnD/_0i7h004P86_/live/dfgwsx/'
df_ts_prefix = 'http://tt-ocjlive.bestvcdn.com.cn/live/program/Docj.live.bestvcdn.com.cnD/_0i7h004P86_/live/dfgwsx/2500000/'


def test_url(url):
    print url
    request = urllib2.Request(url)
    response = urllib2.urlopen(request, timeout=10)
    print 'status: ', response.getcode()
    print response.info()
    return response

def test_ts(url):
    response = test_url(url)

def test_m3u8(url):
    response = test_url(url)
    contents = response.read()
    print contents
    contents = contents.split('\n')

    url_prefix = base_url(response.geturl())
    for line in contents:
        next_url = None
        if 'm3u8' in line:
            next_url = url_prefix + line
            test_m3u8(next_url)
        elif 'ts' in line:
            next_url = url_prefix + line
            test_ts(next_url)
    print('-----------------------------')

def base_url(url):
    if not url or len(url) == 0:
        return ''

    return url[0:url.rindex('/') + 1]

if __name__ == '__main__':
    while True:
        try:
            print('=============================')
            print time.asctime( time.localtime(time.time()) )
            test_m3u8(df_url)
            print('=============================')
            print
            print
            sys.stdout.flush()
            time.sleep(0.2)
        except Exception as e:
            print e
