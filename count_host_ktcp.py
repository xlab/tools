import re
import json

if __name__ == '__main__':
    file_content = None
    with open('qq_retry.txt','r') as f:
        file_content = f.read()

    host_list = re.findall('http://([^/]*)', file_content)
    host_count = {}
    for url in host_list:
        if url in host_count:
            host_count[url] = host_count[url] + 1
        else:
            host_count[url] = 1

    with open('qq_ip_count_ktcp.json', 'w') as outfile:
        json.dump(host_count, outfile)

