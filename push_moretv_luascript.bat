REM call root_adb.bat
set lua_version=2.32.99
set target_dir="/data/data/com.moretv.android/files/luas/%lua_version%"
REM set target_dir="/data/data/com.moretv.kids/files/luas/%lua_version%"
set target_dir="/data/data/com.eagle.live/files/luas/%lua_version%"
REM set target_dir="/data/data/com.moretv.middleware/files/luas/%lua_version%"
REM set target_dir="/data/data/com.moons.tether.wifi/files/luas/%lua_version%"
REM set target_dir="/data/data/com.eagleapp.screen/files/luas/%lua_version%"
set target_dir="/data/data/com.utv.android/files/luas/%lua_version%"
adb shell rm -rf %target_dir%/*
adb push Z:\data\lua_auto_upgrade\autoEnc\scripts\encrypted\ %target_dir%
REM adb push W:\parser\autoEnc\scripts\encrypted\ %target_dir%
adb shell mv -f %target_dir%/encrypted/* %target_dir%
adb shell rm -rf %target_dir%/encrypted/
