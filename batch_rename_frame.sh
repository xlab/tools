#!/bin/bash

suffix="_bai"
for am in `ls *.ExportJson`
do
	filename=`echo "$am" | sed "s/\(.*\)\.ExportJson/\1/"`
	imgsConf="$filename"0.plist
	echo "$filename" - "$am" - "$imgsConf"

	# rename frame name
	sed -i "/armature_data/,/animation_data/ s/\([^\"]*\)\.png/\1$suffix\.png/" "$am"
	sed -i "/texture_data/,/config_file_path/ s/\"name\": \"\([^\"]*\)\"/\"name\": \"\1$suffix\"/" "$am"
	sed -i "/<key>frames<\/key>/,/<key>metadata<\/key>/ s/\([^\"]*\)\.png/\1$suffix\.png/" "$imgsConf"

	# rename filename
	sed -i "/\"version\": 1\.1/,/\"bone_data\":/ s/\"name\": \"\([^\"]*\)\"/\"name\": \"\1$suffix\"/" "$am"
	#sed -i "/animation_data/,/mov_data/ s/\"name\": \"\([^\"]*\)\"/\"name\": \"\1$suffix\"/" "$am"
	#sed -i "/config_file_path/,/]/ s/\"\([^\.]*\)\.plist/\"\1$suffix\.plist/" "$am"	
	#mv "$am" "$filename$suffix".ExportJson
	#sed -i "/<key>metadata<\/key>/,/<\/dict>/ s/\([^>^\.]*\)\.png/\1$suffix\.png/" "$imgsConf"
	#mv "$imgsConf" "$filename"0"$suffix".plist
	#mv "$filename"0.png "$filename"0"$suffix".png
done
