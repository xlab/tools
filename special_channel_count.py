# -*- coding: utf-8 -*-
import re
import json
import urllib2
import traceback
import csv

SID_FILE = 'sids.json'
PLAY_URL_FILE = 'urls.json'
SPECIAL_CHANNEL_JSON_FILE = 'special_channel.json'
SPECIAL_CHANNEL_FILE = 'special_channel.csv'
tv_api = 'http://vod.tvmore.com.cn/Service/V2/queryPrograms?contentType=tv&code=tv_genbo&pageSize=10&pageIndex=1&type=1'
channel_api_fmt = 'http://vod.tvmore.com.cn/Service/V2/queryPrograms?contentType={type}&code={code}&pageSize=50&pageIndex=1&type={type_num}'
channel_api_fmt2 = 'http://vod.tvmore.com.cn/Service/V2/container?contentType={type}&code={code}&pageSize=50&pageIndex=1&type={type_num}'
vod_api_fmt = 'http://vod.tvmore.com.cn/Service/V5/Program?sid={sid}'
# api_fmt = 'http://vod.aginomoto.com/Service/V3/Program?sid={sid}'
failed_urls = []
video_sid_list = []
episode_sid_list = []
result_list = []
CHANNEL_LIST = [
    {'type': 'tv', 'code':'tv_guowai_lianzai', 'name': u'海外同步', 'type_num': 3, 'api': 0},
    {'type': 'tv', 'code':'1_tv_area_xianggang', 'name': u'香港TVB', 'type_num': 3, 'api': 1},
    {'type': 'tv', 'code':'1_tv_area_oumei', 'name': u'特色美剧', 'type_num': 3, 'api': 1},
    {'type': 'tv', 'code':'tv_yueyu', 'name': u'粤语专区', 'type_num': 2, 'api': 1},
    {'type': 'tv', 'code':'1_tv_area_hanguo', 'name': u'韩剧热流', 'type_num': 3, 'api': 1},
    {'type': 'tv', 'code':'1_tv_area_riben', 'name': u'日剧集锦', 'type_num': 3, 'api': 1},
    {'type': 'tv', 'code':'1_tv_area_yingguo', 'name': u'英伦佳剧', 'type_num': 3, 'api': 1},
    {'type': 'comic', 'code':'dongman_xinfan', 'name': u'新番上档', 'type_num': 0, 'api': 0},
]
KEY_SID = 'sid'
KEY_TITLE = 'title'
KEY_SOURCE = 'source'
KEY_URL = 'url'
KEY_CHANNEL = 'channel'
KEY_CODE = 'code'
KEY_CODE_NAME = 'code_name'
KEY_CHANNEL_NAME = 'channel_name'

current_channel = None

def has_media_file(js):
    return js and js['data'] and js['data']['mediaFiles'] and len(js['data']['mediaFiles']) > 0

def channel_consumer(channel):
    global video_sid_list
    channel_type = channel['type']
    code = channel['code']
    type_num = channel['type_num']
    api_fmt = channel_api_fmt if channel['api'] == 0 else channel_api_fmt2
    api = api_fmt.format(type=channel_type, code=code, type_num=type_num)
    request = urllib2.Request(api)
    response = urllib2.urlopen(request)
    try:
        js = json.loads(response.read())
        if int(js['status']) != 200:
            print('channel type: {channel_type} code: {code} status: {status}'.format(channel_type=channel_type, code=code, status=js['status']))
        else:
            if js['data'] and js['data'][0] and js['data'][0]['items']:
                map(channel_item_consumer, js['data'][0]['items'])
            else:
                print('channel code: {code}, data error! url={url}'.format(code=code, url=api))
    except Exception, e:
        traceback.print_exc()
        traceback.print_stack()

def channel_item_consumer(item):
    global video_sid_list
    if item and item['sid']:
        video_sid_list.append(item['sid'])


def video_sid_consumer(sid):
    global episode_sid_list
    api = vod_api_fmt.format(sid=sid)
    request = urllib2.Request(api)
    response = urllib2.urlopen(request)
    try:
        js = json.loads(response.read())
        if int(js['status']) != 200:
            print('sid: {sid}, status: {status}'.format(sid=sid, status=js['status']))
        else:
            if js['data'] and js['data']['episodes']:
                map(episode_consumer, js['data']['episodes'])
            elif has_media_file(js):
                map(media_files_consumer, js['data']['mediaFiles'])
            else:
                print('sid: {sid}, data error!'.format(sid=sid))
    except Exception, e:
        traceback.print_exc()
        traceback.print_stack()

def episode_sid_consumer(sid):
    api = vod_api_fmt.format(sid=sid)
    request = urllib2.Request(api)
    response = urllib2.urlopen(request)
    try:
        js_str = response.read()
        js_str = js_str.replace('\n', '').replace('\r', '').replace('\t', '')
        js = json.loads(js_str)
        if int(js['status']) != 200:
            print('sid: {sid}, status: {status}'.format(sid=sid, status=js['status']))
        else:
            if has_media_file(js):
                result_list = map(media_files_consumer, js['data']['mediaFiles'])
                for result in result_list:
                    result[KEY_SID] = sid
                    result[KEY_TITLE] = js['data']['metadata']['title']
            else:
                print('sid: {sid}, data error!'.format(sid=sid))
    except Exception, e:
        traceback.print_exc()
        traceback.print_stack()
        # print('sid: {sid}, response error:{js_str}'.format(sid=sid, js_str=js_str))

def episode_consumer(episode):
    global episode_sid_list
    if episode and episode['sid']:
        episode_sid_list.append(episode['sid'])

def media_files_consumer(media_file):
    global result_list
    result_entry = {}
    if media_file:
        result_entry[KEY_SOURCE] = media_file['source']
        result_entry[KEY_CHANNEL] = current_channel['type']
        result_entry[KEY_CODE] = current_channel['code']
        result_entry[KEY_CHANNEL_NAME] = current_channel['type']
        result_entry[KEY_CODE_NAME] = current_channel['code']
        result_entry[KEY_URL] = media_file['url']
        result_entry[KEY_SID] = ''
        result_list.append(result_entry)
        print result_entry
    return result_entry

if __name__ == '__main__':
    # with open(PLAY_URL_FILE, 'w') as f:
    #     json.dump(failed_urls, f)
    # map(channel_consumer, CHANNEL_LIST)
    for channel in CHANNEL_LIST:
        current_channel = channel
        channel_consumer(channel)
        map(video_sid_consumer, video_sid_list)
        map(episode_sid_consumer, episode_sid_list)
        del video_sid_list[:]
        del episode_sid_list[:]

    print 'found program size:', len(result_list)
    keys = [KEY_CHANNEL, KEY_CODE, KEY_TITLE, KEY_SID, KEY_URL, KEY_SOURCE]
    with open(SPECIAL_CHANNEL_JSON_FILE, 'w') as f:
        json.dump(result_list, f)

    with open(SPECIAL_CHANNEL_FILE, 'wb') as f:
        csv_w = csv.writer( f )
        csv_w.writerow( keys )

        for result in result_list:
            csv_w.writerow( map( lambda x: unicode(entry.get( x, "" )).encode("utf-8"), keys ) )

