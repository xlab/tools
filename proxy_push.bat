@echo off
if [%1]==[] goto usage
if [%2]==[] goto usage

set proxy_dir=/data/local/tmp
set target_dir=%2
echo proxy: %proxy_dir%/%1
echo target: %target_dir%/%1
echo adb push %1 %proxy_dir%
adb push %1 %proxy_dir%
echo adb shell mv %proxy_dir%/%1  %target_dir%/%1
adb shell mv %proxy_dir%/%1  %target_dir%/%1
@echo Done.
goto :eof

:usage
@echo Usage: %0 file target_dir
