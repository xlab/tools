# -*- coding: utf-8 -*-
import re
import json
import urllib2
import traceback
import csv

INPUT_FILE= 'special_channel.json'
OUTPUT_FILE = 'special_channel.csv'

if __name__ == '__main__':
    file_content = None
    with open(INPUT_FILE, 'r') as f:
        file_content = f.read()

    if not file_content:
        print 'file empty!'
        exit(-1)

    input_json = json.loads(file_content)

    if len(input_json) <= 0:
        print 'json empty!'
        exit(-1)

    keys = input_json[0].keys()
    print(keys)

    with open(OUTPUT_FILE, 'wb') as f:
        csv_w = csv.writer( f )
        csv_w.writerow( keys )

        index = 0
        for entry in input_json:
            print index, entry['sid']
            csv_w.writerow( map( lambda x: unicode(entry.get( x, "" )).encode("utf-8"), keys ) )
            index = index + 1

