import re
import json

if __name__ == '__main__':
    file_content = None
    with open('urls_input.json','r') as f:
        file_content = f.read()

    host_list = re.findall('(http://\S+)', file_content)
    # host_list = re.findall('(http://[^\'^\"]+)', file_content)

    with open('urls.json', 'w') as outfile:
        json.dump(host_list, outfile)

