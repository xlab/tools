import re
import json
import urllib2

SID_FILE = 'sids.json'
PLAY_URL_FILE = 'urls.json'
api_fmt = 'http://vod.tvmore.com.cn/Service/V4/Program?sid={sid}'
# api_fmt = 'http://vod.aginomoto.com/Service/V3/Program?sid={sid}'
if __name__ == '__main__':
    file_content = None
    with open(SID_FILE, 'r') as f:
        file_content = f.read()

    # print file_content
    failed_sids = json.loads(file_content)
    failed_urls = []

    # print failed_sids
    for failed_entry in failed_sids:
        api = api_fmt.format(sid=failed_entry)
        request = urllib2.Request(api)
        response = urllib2.urlopen(request)
        try:
            js = json.loads(response.read())
            if int(js['status']) == 200:
                print failed_entry
            else:
                failed_urls.append(js['program']['mediaFiles'][0]['url'])
                print js['program']['metadata']['title'], js['status']
        except Exception, e:
            print e
        else:
            pass

    # failed_urls = list(set(failed_urls))

    with open(PLAY_URL_FILE, 'w') as f:
        json.dump(failed_urls, f)

