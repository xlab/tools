import re
import json
import urllib2

save_urls = True
input_file = 'funshion.json'
output_file='url_results.json'
if __name__ == '__main__':
    file_content = None
    with open(input_file, 'r') as f:
        file_content = f.read()

    input_json = json.loads(file_content)
    results = []
    for input_item in input_json:
        if input_item['count'] > 100:
            url = input_item['page']
            if save_urls:
                results.append(url)
            else:
                try:
                    request = urllib2.Request(url)
                    response = urllib2.urlopen(request)
                    if response.getcode() != 200:
                        entry = {}
                        entry['page'] = url
                        entry['status'] = response.getcode()
                        results.append(entry)
                        print response.getcode(), entry
                    else:
                        print 200, url
                except Exception, e:
                    print e

    with open(output_file, 'w') as f:
        json.dump(results, f)

