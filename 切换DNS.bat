@echo off
set A_dns1=172.249.0.1
set A_dns2=8.8.8.8
set B_dns1=172.249.0.24
set B_dns2=8.8.8.8
:start
cls
@echo off
echo ======================================================
echo A.进入外网 - 设置本地连接DNS为172.249.0.1
echo B.测试环境 - 设置本地连接DNS为172.249.0.24
echo C.退出
echo ======================================================
set /P temp= 请选择：
if /I "%temp%"=="A" goto :set_A
if /I "%temp%"=="B" goto :set_B
if /I "%temp%"=="C" goto :end
goto :start
:set_A
cls
echo 进入外网 - 正在设置本地连接DNS为172.249.0.1 ...
netsh interface ip set dns name="本地连接" source=static addr=%A_dns1%
netsh interface ip add dns "本地连接" %A_dns2% index=2
echo 正在清除DNS解析缓存...
ipconfig /flushdns
echo 完成，已切换至外网环境。
pause >nul
goto :end
:set_B
cls
echo 进入测试环境 - 正在设置本地连接DNS为172.249.0.24 ...
netsh interface ip set dns name="本地连接" source=static addr=%B_dns1%
netsh interface ip add dns "本地连接" %B_dns2% index=2
echo 正在清除DNS解析缓存...
ipconfig /flushdns
echo 完成，已切换至内网测试环境。
pause >nul
goto :end
:end
