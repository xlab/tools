from operator import xor

pkg_code = 0xd8
salt_code = 0xa9
package_name = 'com.heytap.tv.market.demo'
salt = 'd59379d2ad8a34f71858fbaabd0dafb3f9638581281d9a8aae1c3a1c0bd8f2f2'


def get_confused(plain, code):
    confused = []
    for c in plain:
        confused.append(xor(ord(c), code))

    return confused


def get_plain(confused, code):
    plain = ''
    for i in confused:
        plain += chr(xor(i, code))

    return plain


def main():
    confused_pkg = get_confused(package_name, pkg_code)
    # print confused_key
    print 'confused package:'
    for i in confused_pkg:
        print '0x%x,' % (i),
    print
    print 'plain pakcage:', get_plain(confused_pkg, pkg_code)

    print ''

    confused_salt = get_confused(salt, salt_code)
    # print confused_key
    print 'confused salt:'
    for i in confused_salt:
        print '0x%x,' % (i),
    print
    print 'plain salt:', get_plain(confused_salt, salt_code)


if __name__ == '__main__':
    main()
