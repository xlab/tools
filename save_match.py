import re
import json

pattern = '([^-^\n]\S+):\s'

if __name__ == '__main__':
    file_content = None
    with open('input.txt','r') as f:
        file_content = f.read()

    match_list = list(set(re.findall(pattern, file_content)))
    print ' '.join(match_list)

    with open('output.txt', 'w') as outfile:
        json.dump(match_list, outfile)

