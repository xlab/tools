console.log("Script loaded successfully!");


function getHexString(data) {
    console.log("getHexString data length: " + data.size());
    var hexstr = '';
    for (var i = 0; i < data.length; i++) {
        b = (data[i] >>> 0) & 0xff;
        n = b.toString(16);
        hexstr += ("00" + n).slice(-2) + " ";
        console.log('getHexString: ' + i);
    }
    return hexstr;
}

Java.perform(function x() {
    var MApplication = Java.use('com.cloudmedia.tv.MApplication');
    var DexClassLoader = Java.use("dalvik.system.DexClassLoader");
    var JString = Java.use("java.lang.String");
    console.log("DexClassLoader: " + DexClassLoader);
    var dexClassLoader = null;
    var targetClassName = JString.$new("com.cloudmedia.tv.plug.ParserURLUtils");
    DexClassLoader.loadClass.overload("java.lang.String").implementation = function(arg1) {
        // console.log("arg1: " + arg1);
        if (targetClassName.equals(arg1)) {
            dexClassLoader = Java.retain(this);
            console.log("dexClassLoader: " + dexClassLoader);
            var classLoader = dexClassLoader;
            Java.classFactory.loader = dexClassLoader;
            var Migu = Java.use('com.b.j');
            console.log("Migu: " + Migu);
            var ByteString = Java.use("com.android.okhttp.okio.ByteString");
            Migu.aV.implementation = function (arg1) {
                console.log("Migu.aV arg1: " + arg1);
                var ret = this.aV(arg1);
                console.log("Migu.aV ret: " + ret);
                return ret;
            };       
         }
        return this.loadClass(arg1);
    };
});