# Written by rotlogix & lavalamp
#
#
import frida
import sys
from subprocess import Popen

# process_name = 'com.tencent.ktsdk.demo'
# process_name = 'com.helios.kids'
process_name = 'com.moretv.android'

def on_message(message, data):
    try:
        if message:
            print("[*] {0}".format(message["payload"]))
    except Exception as e:
        print(message)
        print(e)

def do_get_key():

    get_key = """
        Java.perform(function () {
            var Activity = Java.use("android.app.Activity");
            Activity.onResume.implementation = function () {
                send("onResume() got called! Let's call the original implementation");
                this.onResume();
            };
        });

        Java.perform(function () {
            var Activity = Java.use("android.app.Activity");
            Activity.onPause.implementation = function () {
                send("onPause() got called! Let's call the original implementation");
                this.onPause();
            };
        });

        Java.perform(function () {

            var CKeyFacade = Java.use("com.tencent.qqlive.ck.CKeyFacade");

            CKeyFacade.CkeyMoudleInit.implementation = function (s1, s2, s3, c) {

                send("CkeyMoudleInit");
                send(s1.toString());
                send(s2.toString());
                send(s3.toString());
                send(c.toString());
                this.CkeyMoudleInit(s1, s2, s3, c);
            };
        });

        Java.perform(function () {

            var CKeyFacade = Java.use("com.tencent.qqlive.ck.CKeyFacade");

            CKeyFacade.getCKey.overload("int", "long", "java.lang.String", "int", "java.lang.String").implementation = function (i, l, s, i2, s2) {

                send("CKeyFacade1");
                send(i.toString());
                send(s.toString());
                send(l.toString());
                send(i2.toString());
                send(s2.toString());
                var result = this.getCKey(i, l, s, i2, s2);
                send(result.toString());
                return result;
            };
        });

        Java.perform(function () {

            var CKeyFacade = Java.use("com.tencent.qqlive.ck.CKeyFacade");

            CKeyFacade.getCKey.overload("int", "long", "java.lang.String", "int", "java.lang.String",  "java.lang.String",  "java.lang.String").implementation = function (i, l, s, i2, s2, s3, s4) {

                send("CKeyFacade2");
                send(i.toString());
                send(s.toString());
                send(l.toString());
                send(i2.toString());
                send(s2.toString());
                var result = this.getCKey(i, l, s, i2, s2, s3, s4);
                send(result.toString());
                return result;
            };
        });

        Java.perform(function () {

            var CKeyFacade = Java.use("com.tencent.qqlive.ck.CKeyFacade");

            CKeyFacade.getCKey.overload("int", "long", "java.lang.String", "int", "java.lang.String",  "java.lang.String",  "java.lang.String", "java.lang.String",  "java.lang.String",  "java.lang.String").implementation = function (i, l, s, i2, s2, s3, s4, s5, s6, s7) {

                send("CKeyFacade3");
                send(i.toString());
                send(s.toString());
                send(l.toString());
                send(i2.toString());
                send(s2.toString());
                var result = this.getCKey(i, l, s, i2, s2, s3, s4, s5, s6, s7);
                send(result.toString());
                return result;
            };
        });

        Java.perform(function () {

            var CKeyFacade = Java.use("com.tencent.qqlive.ck.CKeyFacade");

            CKeyFacade.getCKey.overload("int", "long", "java.lang.String", "int", "java.lang.String",  "java.lang.String", "java.lang.String", "java.lang.String").implementation = function (i, l, s, i2, s2, s3, s4, s5) {

                send("CKeyFacade4");
                send(i.toString());
                send(s.toString());
                send(l.toString());
                send(i2.toString());
                send(s2.toString());
                var result = this.getCKey(i, l, s, i2, s2, s3, s4, s5);
                send(result.toString());
                return result;
            };
        });

        Java.perform(function () {

            var CKeyFacade = Java.use("com.tencent.qqlive.ck.CKeyFacade");

            CKeyFacade.getCKey.overload("int", "long", "java.lang.String", "int", "java.lang.String",  "java.lang.String", "java.lang.String",  "java.lang.String", "java.lang.String", "[I", "int").implementation = function (i, l, s, i2, s2, s3, s4, s5, s6, ia, i3) {

                send("CKeyFacade5");
                send(i.toString());
                send(l.toString());
                send(s.toString());
                send(i2.toString());
                send(s2.toString());
                send(s3.toString());
                send(s4.toString());
                send(s5.toString());
                send(s6.toString());
                send(ia.toString());
                send(i3.toString());
                var result = this.getCKey(i, l, s, i2, s2, s3, s4, s5, s6, ia, i3);
                send(result.toString());
                return result;
            };
        });

    """
    return get_key


        # Java.perform(function () {
        #     var CKeyFacade = Java.use("com.tencent.qqlive.ck.CKeyFacade");

        #     CKeyFacade.GenCKey.overload("java.lang.String", "long", "java.lang.String", "int", "int", "int", "java.lang.String", "java.lang.String", "int[]", "int").implementation = function (paramString1, paramLong,
        #     paramString2, paramInt1, paramInt2, paramInt3,
        #     paramString3, paramString4, paramArrayOfInt,
        #     paramInt4) {

        #         send("CKeyFacade");
        #         send(paramString1.toString());
        #         send(paramLong.toString());
        #         send(paramString2.toString());
        #         var result = this.GenCKey(paramString1, paramLong,
        #         paramString2, paramInt1, paramInt2, paramInt3,
        #         paramString3, paramString4, paramArrayOfInt,
        #         paramInt4);
        #         send(result.toString());
        #         return result;
        #     };
        # });



if __name__ == "__main__":
    try:
         Popen(["adb forward tcp:27042 tcp:27042"], shell=True).wait()
         process = frida.get_device_manager().enumerate_devices()[-1].attach(process_name)
         script = process.create_script(do_get_key())
         script.on('message', on_message)
         script.load()
         sys.stdin.read()
    except KeyboardInterrupt as e:
        sys.exit(0)
