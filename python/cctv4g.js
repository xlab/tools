console.log("Script loaded successfully 55");


Java.perform(function x() {
    var LuaManager = Java.use("com.eris.video.luatojava.LuaManager");
    LuaManager.exec.implementation = function (s1, s2, s3, b1, s4) {
        send('exec s1:' + s1);
        send('exec s2:' + s2);
        send('exec s3:' + s3);
        send('exec b1:' + b1);
        send('exec s4:' + s4);
        var ret = this.exec(s1, s2, s3, b1, s4);
        send('exec ret:' + ret);
        return ret;
    }
});
