# Written by rotlogix & lavalamp
#
#
import frida
import sys
from subprocess import Popen

process_name = 'com.sohu.tv'
# process_name = 'com.android.chrome'

def on_message(message, data):
    try:
        if message:
            print("[*] {0}".format(message["payload"]))
    except Exception as e:
        print(message)
        print(e)

def do_get_key():

    get_key = """
Java.perform(function () {
    var Activity = Java.use("android.app.Activity");
    Activity.onResume.implementation = function () {
        send("onResume() got called! Let's call the original implementation");
        this.onResume();
    };
});
        Java.perform(function () {

                var DCHelper = Java.use("com.sohu.sohuvideo.control.jni.DCHelper");

                DCHelper.getKey.implementation = function (c, i, i2, s, s2, s3) {

                    send("DCHelper");
                    send(c.toString());
                    send(i.toString());
                    send(i2.toString());
                    send(s.toString());
                    send(s2.toString());
                    send(s3.toString());
                    var result = this.getKey(c, i, i2, s, s2, s3);
                    send(result.toString());
                    return result;
                };
        });

    """
    return get_key

if __name__ == "__main__":
    try:
         Popen(["adb forward tcp:27042 tcp:27042"], shell=True).wait()
         process = frida.get_device_manager().enumerate_devices()[-1].attach(process_name)
         script = process.create_script(do_get_key())
         script.on('message', on_message)
         script.load()
         sys.stdin.read()
    except KeyboardInterrupt as e:
        sys.exit(0)
