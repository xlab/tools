Java.perform(function () {

    var CKeyFacade = Java.use("com.tencent.qqlive.ck.CKeyFacade");

    CKeyFacade.init.overload("android.content.Context", "java.lang.String",  "java.lang.String").implementation = function (c, s1, s2) {

        send("init");
        send(c.toString());
        send(s1.toString());
        send(s2.toString());
        send("this.g:" + this.g);
        this.init(c, s1, s2);
    };

    // CKeyFacade.getCKey.overload("int", "long", "java.lang.String", "int", "java.lang.String").implementation = function (i, l, s, i2, s2) {

    //     send("CKeyFacade1");
    //     send(i.toString());
    //     send(s.toString());
    //     send(l.toString());
    //     send(i2.toString());
    //     send(s2.toString());
    //     var result = this.getCKey(i, l, s, i2, s2);
    //     send(result.toString());
    //     return result;
    // };

    CKeyFacade.getCKey.overload("int", "long", "java.lang.String", "int", "java.lang.String",  "java.lang.String",  "java.lang.String").implementation = function (i, l, s, i2, s2, s3, s4) {

        send("CKeyFacade2");
        send(i.toString());
        send(s.toString());
        send(l.toString());
        send(i2.toString());
        send(s2.toString());
        var result = this.getCKey(i, l, s, i2, s2, s3, s4);
        send(result.toString());
        return result;
    };

    CKeyFacade.getCKey.overload("int", "long", "java.lang.String", "int", "java.lang.String",  "java.lang.String",  "java.lang.String", "java.lang.String",  "java.lang.String",  "java.lang.String").implementation = function (i, l, s, i2, s2, s3, s4, s5, s6, s7) {

        send("CKeyFacade3");
        send(i.toString());
        send(s.toString());
        send(l.toString());
        send(i2.toString());
        send(s2.toString());
        var result = this.getCKey(i, l, s, i2, s2, s3, s4, s5, s6, s7);
        send(result.toString());
        return result;
    };

    // CKeyFacade.getCKey.overload("int", "long", "java.lang.String", "int", "java.lang.String",  "java.lang.String", "java.lang.String", "java.lang.String").implementation = function (i, l, s, i2, s2, s3, s4, s5) {

    //     send("CKeyFacade4");
    //     send(i.toString());
    //     send(s.toString());
    //     send(l.toString());
    //     send(i2.toString());
    //     send(s2.toString());
    //     var result = this.getCKey(i, l, s, i2, s2, s3, s4, s5);
    //     send(result.toString());
    //     return result;
    // };

    CKeyFacade.getCKey.overload("int", "long", "java.lang.String", "int", "java.lang.String",  "java.lang.String", "java.lang.String",  "java.lang.String", "java.lang.String", "[I", "int").implementation = function (i, l, s, i2, s2, s3, s4, s5, s6, ia, i3) {
        send("CModuleInit")
        send("this.g:" + this.g.value);
        send("this.b:" + this.b.value);
        send("this.a:" + this.a.value);
        send("CKeyFacade.SetIpPort(" + this.h.value + ", " + this.j.value + ", " +  this.i.value + ", " + this.k.value +  ")");
        send('Registration')
        send('com.tencent.qqlive.mediaplayer.logic.h.f(): ' + h.f().toString())
        send('com.tencent.qqlive.mediaplayer.i.n.a(h.a(), 0): ' + n.a(h.a(), 0).toString())
        send("CKeyFacade5");
        send(i.toString());
        send(l.toString());
        send(s.toString());
        send(i2.toString());
        send(s2.toString());
        send(s3.toString());
        send(s4.toString());
        send(s5.toString());
        send(s6.toString());
        send(ia.toString());
        send(i3.toString());
        var result = this.getCKey(i, l, s, i2, s2, s3, s4, s5, s6, ia, i3);
        send(result.toString());
        return result;
    };

    var o = Java.use("com.tencent.qqlive.mediaplayer.i.o");
    var n = Java.use("com.tencent.qqlive.mediaplayer.i.n");
    var h = Java.use("com.tencent.qqlive.mediaplayer.logic.h");
    h.f.overload().implementation = function() {
        var ret = this.f();
        // send("h.f() ret:" + ret)
        return ret;
    }
});
