import json
import requests
from urllib.parse import quote


COOKIE = '''wsessionid=6-9P4BO77JOQPGM3ZW0Y1HYM99; _csrf=34db581b1442838da67cb9a1b8e7e62faefaee4d605a5ab408ca23e21980936ea%3A2%3A%7Bi%3A0%3Bs%3A5%3A%22_csrf%22%3Bi%3A1%3Bs%3A32%3A%22ORqIKG97E97757cnoj2Fvrb8OzvHj-c9%22%3B%7D; PHPSESSID=pk78locagd7r4tisovt9ia0k76; _identity=b236d62017004ba6ed7031c6e88a00cd0a3a97ce4688d17da02226fd154f5dd8a%3A2%3A%7Bi%3A0%3Bs%3A9%3A%22_identity%22%3Bi%3A1%3Bs%3A47%3A%22%5B30%2C%22rjWA_zMHdbi0D1dxyMCCNQZdEfgEpeR5%22%2C2592000%5D%22%3B%7D'''
CSRF = 'aY6yZOoTfyqw4O2QffE0yZwF1-lN6Rov9I6CllmMVhom3MMtoVRGHfXZ2qdIxlen82_lrzubeBe79PTeM6E1Iw=='
CHANNEL_SOURCE_CONFIG = 'hzwasu_cctv.json'
CHANNEL_SOURCE_CONFIG = 'hzwasu_difang.json'
TEST_ENV = True
TEST_IP = '106.75.66.130'
PROD_IP = 'new-mlive.tvmore.com.cn'


def log(tag, message):
    print('[' + tag + '] ' + str(message))


def post(url, body, headers):
    if not url or not url.startswith('http'):
        return None

    headers = headers or {}
    log('post', body)
    r = requests.post(url, data=body.encode('utf-8'), headers=headers)
    return r


def read_from_file(file_name):
    if is_empty(file_name):
        return ''

    content = ''
    with open(file_name, 'r', encoding="utf-8") as infile:
        content = infile.read()

    return content


def is_empty(text):
    return not text or len(text) == 0


def submit_channel(channel_info):
    log('submit_channel', channel_info)
    if is_empty(channel_info):
        log('submit_channel', 'channel is empty')
        return

    if not isinstance(channel_info, dict):
        log('submit_channel', 'channel is not dict')
        return

    data_fmt = '_csrf={csrf}&MtvSource[code]={channel_code}&MtvSource[site]={site}&MtvSource[url]={url}&MtvSource[needmac]=0&MtvSource[alias]=&MtvSource[weight]={weight}&MtvSource[type]=0&MtvSource[bitrate]={bitrate}&MtvSource[status]={status}&MtvSource[timeshift]=1'
    data = None
    if 'channel_code' in channel_info and 'site' in channel_info and 'url' in channel_info and 'weight' in channel_info and 'bitrate' in channel_info and 'status' in channel_info:
        data = data_fmt.format(csrf=quote(CSRF), channel_code=channel_info['channel_code'], site=channel_info['site'], url=quote(channel_info['url']), weight=channel_info['weight'], bitrate=channel_info['bitrate'], status=channel_info['status'])
    else:
        log('submit_channel', 'channel is invalid')
        return

    api_fmt = 'http://{ip}/mtv-source/add'
    url = api_fmt.format(ip=(TEST_IP if TEST_ENV else PROD_IP))
    headers = {}
    headers['Referer'] = 'http://new-mlive.tvmore.com.cn/mtv-source/index?code=ugbmhh'
    headers['Cookie'] = COOKIE
    headers['Host'] = 'new-mlive.tvmore.com.cn'
    headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
    result = post(url, data, headers)
    log('submit_channel', 'result ' + str(result))
    log('submit_channel', 'result content ' + str(result.text))


def batch_add_source():
    log('batch_add_source', 'start ' + CHANNEL_SOURCE_CONFIG)
    conf_str = read_from_file(CHANNEL_SOURCE_CONFIG)
    if is_empty(conf_str):
        log('batch_add_source', 'conf_str is empty')
        return

    conf_obj = json.loads(conf_str)
    if is_empty(conf_obj):
        log('batch_add_source', 'conf_obj is empty')
        return

    log('batch_add_source', 'conf_obj lenth: ' + str(len(conf_obj)))
    for channel_info in conf_obj:
        submit_channel(channel_info)

    log('batch_add_source', 'end')


def main():
    log('main', '==== start ====')
    batch_add_source()
    log('main', '==== end ====')


if __name__ == '__main__':
    main()
