import time
import frida
import json
import sys
from subprocess import Popen

process_name = 'com.youku.phone'
target_js_file = 'cipher.js'

enc_cipher_hashcodes = [] #cipher objects with Cipher.ENCRYPT_MODE will be stored here
dec_cipher_hashcodes = [] #cipher objects with Cipher.DECRYPT_MODE will be stored here

def on_message(message, payload):
    # mainly printing the data sent from the js code, and managing the cipher objects according to their operation mode
    if message["type"] == "send":
        # print message["payload"]
        my_json = json.loads(message["payload"])
        if my_json["my_type"] == "KEY":
            print "Key sent to SecretKeySpec()", payload.encode("base64")
        elif my_json["my_type"] == "IV":
            print "Iv sent to IvParameterSpec()", payload.encode("base64")
        elif my_json["my_type"] == "hashcode_enc":
            enc_cipher_hashcodes.append(my_json["hashcode"])
        elif my_json["my_type"] == "hashcode_dec":
            dec_cipher_hashcodes.append(my_json["hashcode"])
        elif my_json["my_type"] == "Key from call to cipher init":
            print "Key sent to cipher init()", payload.encode("base64")
        elif my_json["my_type"] == "IV from call to cipher init":
            print "Iv sent to cipher init()", payload.encode("base64")
        elif my_json["my_type"] == "before_doFinal" and my_json["hashcode"] in enc_cipher_hashcodes:
            #if the cipher object has Cipher.MODE_ENCRYPT as the operation mode, the data before doFinal will be printed
            #and the data returned (ciphertext) will be ignored
            print "Data to be encrypted :", payload.encode("base64"), type(payload)
        elif my_json["my_type"] == "after_doFinal" and my_json["hashcode"] in enc_cipher_hashcodes:
            print "Encrypted :", payload.encode("base64"), type(payload)
        elif my_json["my_type"] == "before_doFinal" and my_json["hashcode"] in dec_cipher_hashcodes:
            print "Data to be decrypted :", payload.encode("base64")
        elif my_json["my_type"] == "after_doFinal" and my_json["hashcode"] in dec_cipher_hashcodes:
            print "Decrypted data :", payload.encode("base64")
    else:
        print message
        print '*' * 16
        print payload

def get_script():
    script = ''
    with open(target_js_file, 'rb') as f:
        script = f.read()

    return script

if __name__ == "__main__":
    try:
        Popen(["adb forward tcp:27042 tcp:27042"], shell=True).wait()
        device = frida.get_device_manager().enumerate_devices()[-1]
        # process = device.attach(device.spawn([process_name]))
        process = device.attach(process_name)
        script = process.create_script(get_script())
        script.on('message', on_message)
        script.load()
        sys.stdin.read()
    except KeyboardInterrupt as e:
        sys.exit(0)
