// Java.perform(function () {
//     var Activity = Java.use('android.app.Activity');
//     Activity.onResume.implementation = function () {
//         send('onResume() got called! Let\'s call the original implementation');
//         this.onResume();
//     };
// });

// Java.perform(function () {
//     var Activity = Java.use('android.app.Activity');
//     Activity.onPause.implementation = function () {
//         send('onPause() got called! Let\'s call the original implementation');
//         this.onPause();
//     };
// });
function sendWithDivider(message) {
    send('==============' + message + '==============');
}

Java.perform(function () {

    var CKeyFacade = Java.use('com.youku.antitheftchain.a.b');

    CKeyFacade.a.implementation = function (o) {
        sendWithDivider('CKeyFacade');
        var fieldArray = this.class.getDeclaredFields();
        for (var i = 0; i < fieldArray.length; i++) {
            send("Name of field " + i + ': '+ fieldArray[i].getName());
            send("Reference of field " + i + ': ' + fieldArray[i].get(this));
            if (fieldArray[i].get(this) != null) {
                send("Value of field " + i + ': ' + fieldArray[i].get(this).value);
                send("Type of field " + i + ': ' + fieldArray[i].get(this).type);
            }
        }
        send('ccode=' + o.f());
        send('client_ip=' + o.c());
        send('client_ts=' + o.d());
        send('utid=' + o.a());
        send('vid=' + o.e());
        var ret = this.a(o);
        send(o);
        send(ret);
        send('\n');
        send(this.a(o));
        return ret;
    };
});

Java.perform(function () {

    var Request = Java.use('com.youku.upsplayer.a');

    Request.a.overload('com.youku.upsplayer.c.b', 'java.util.Map', 'com.youku.upsplayer.c.a', 'com.youku.upsplayer.b').implementation = function (pb, pm, pa, pb2) {
        sendWithDivider('Request 1');
        var ret = this.a(pb, pm, pa, pb2);
        send('client_ip=' + pb.jdField_b_of_type_JavaLangString);
        send('utid=' + pb.d.toString());
        send('client_ts=' + pb.c);
        return ret;
    };
});

Java.perform(function () {

    var Request = Java.use('com.youku.upsplayer.a');

    Request.a.overload('com.youku.upsplayer.c.b', 'java.util.Map', 'com.youku.upsplayer.c.a', 'com.youku.upsplayer.b').implementation = function (pb, pm, pa, pb2) {
        sendWithDivider('Request 2');
        var ret = this.a(pb, pm, pa, pb2);
        send('client_ip=' + pb.jdField_b_of_type_JavaLangString);
        send('utid=' + pb.d.toString());
        send('client_ts=' + pb.c);
        return ret;
    };
});

Java.perform(function () {

    var AVMP = Java.use('com.alibaba.wireless.security.framework.e');

    AVMP.b.overload('android.content.Context').implementation = function (c) {
        sendWithDivider('PluginFramework');
        var ret = this.b(c);
        new Exception().printStackTrace();
        return ret;
    };
});

Java.perform(function () {

    var SecurityGuardAVMPPlugin = Java.use('com.alibaba.wireless.security.mainplugin.a');

    SecurityGuardAVMPPlugin.doCommand.implementation = function (o) {
        sendWithDivider('SecurityGuardAVMPPlugin');
        var ret = this.doCommand(o);
        send('param:' + o);
        return ret;
    };
});
