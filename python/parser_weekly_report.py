# -*- coding: utf8 -*-
import re
import sys
import codecs

debug = True
debug = False

month_map = {
    '/一月/': '/01/',
    '/二月/': '/02/',
    '/三月/': '/03/',
    '/四月/': '/04/',
    '/五月/': '/05/',
    '/六月/': '/06/',
    '/七月/': '/07/',
    '/八月/': '/08/',
    '/九月/': '/09/',
    '/十月/': '/10/',
    '/十一月/': '/11/',
    '/十二月/': '/12/',
}

name_map = {
    'xie.junliang': '谢俊良',
    'chen.jiangyong': '陈江勇',
    'liu.yong': '刘勇',
    'wangliu.haoyu': '王留浩裕',
}

def convert_map(text, maps):
    if not text:
        return None

    for source_str in maps.keys():
        text = text.replace(source_str, maps[source_str])

    return text

def target_filename(filename):
    if not filename:
        return None

    return re.sub(r'([^.]*)\.', r'\1_converted.', filename)

def convert_date(text):
    if not text:
        return None

    return re.sub(r'(\d\d)\/([^\\][^\\])\/(\d\d)', r'\3/\2/\1', text)


def main():
    if len(sys.argv) <= 1:
        print('Filename must be provided!')

    filename = sys.argv[1]

    with open(filename, 'r') as source_file, open(target_filename(filename), 'w') as target_file:
        target_file.write(codecs.BOM_UTF8)
        for line in source_file:
            target_line = convert_map(line, month_map)
            target_line = convert_map(target_line, name_map)
            target_line = convert_date(target_line)
            target_file.write(target_line)



def test_main():
    test_text = '任务,YYZJJ-683,114255,深圳电视台app直播解析,xie.junliang,xie.junliang,Medium,完成,完成,05/一月/18 3:39 下午,06/一月/18 12:17 下午,'
    new_test_text = convert_month(test_text)
    print('convert_month:', new_test_text)
    new_test_text = convert_date(new_test_text)
    print('convert_date:', new_test_text)
    print(test_text)
    filename = 'a.csv'
    print('filename:', filename)
    print('target_filename:', target_filename(filename))

if __name__ == '__main__':
    if debug:
        test_main()
    else:
        main()
