// Interceptor.attach(Module.findExportByName(null, "dlsym"), {
//     onEnter: function(args) {
//         console.log("dlsym onEnter..." + Memory.readCString(ptr(args[0])) + ' symbol:' + Memory.readCString(ptr(args[1])));
//     }
// });

// Interceptor.attach(Module.findExportByName(null, "open"), {
//     onEnter: function(args) {
//         console.log("open onEnter..." + Memory.readCString(ptr(args[0])));
//     }
// });

// Interceptor.attach(Module.findExportByName(null, "unlink"), {
//     onEnter: function(args) {
//         console.log("unlink onEnter..." + Memory.readCString(ptr(args[0])));
//     }
// });

// var unlinkPtr = Module.findExportByName(null, 'unlink'); 

// Interceptor.replace(unlinkPtr, new NativeCallback(function (){  
//     // console.log("[*] unlink() encountered, skipping it.");
// }, 'int', []));
var logTimeCall = false;
function getMiGuSign() {
    Java.perform(function x() {
        var MGEncryptor = Java.use("com.cmcc.migutv.encryptor.MGEncryptor");
        var ApplicationWrapper = Java.use("com.secneo.apkwrapper.ApplicationWrapper");
        var Context = Java.use("android.content.Context");
        var Application = Java.use("android.app.Application");
        var realApplication = ApplicationWrapper.realApplication;
        console.log("realApplication: " + JSON.stringify(realApplication));
        var context = Java.cast(realApplication.value, Application);
        logTimeCall = true;
        var result = MGEncryptor.$new().getMiGuSign(context, "aaa");
        logTimeCall = false;
        console.log("result: " + result);
    });
}
Java.perform(function x() {
    var System = Java.use("java.lang.System");
    System.currentTimeMillis.implementation = function () {
        if (false && logTimeCall) {
            Java.perform(function () {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()))
            });
        }
        var ret = this.currentTimeMillis();
        return ret;
    };
});

Interceptor.attach(Module.findExportByName(null, "gettimeofday"), {
    onEnter: function (args) {
        if (false && logTimeCall) {
            console.log("gettimeofday onEnter begin...");
            var backtrace = Thread.backtrace(this.context, Backtracer.ACCURATE).map(DebugSymbol.fromAddress).join("\n\t");
            console.log("Backtrace:" + backtrace);
            console.log("gettimeofday onEnter end...");
        }
    },
});

// list all registed method
// var RevealNativeMethods = function () {
//     var pSize = Process.pointerSize;
//     var env = Java.vm.getEnv();
//     var RegisterNatives = 215, FindClassIndex = 6; // search "215" @ https://docs.oracle.com/javase/8/docs/technotes/guides/jni/spec/functions.html
//     var jclassAddress2NameMap = {};
//     function getNativeAddress(idx) {
//         return env.handle.readPointer().add(idx * pSize).readPointer();
//     }
//     // intercepting FindClass to populate Map<address, jclass>
//     Interceptor.attach(getNativeAddress(FindClassIndex), {
//         onEnter: function (args) {
//             jclassAddress2NameMap[args[0]] = args[1].readCString();
//         }
//     });
//     // RegisterNative(jClass*, .., JNINativeMethod *methods[nMethods], uint nMethods) // https://android.googlesource.com/platform/libnativehelper/+/master/include_jni/jni.h#977
//     Interceptor.attach(getNativeAddress(RegisterNatives), {
//         onEnter: function (args) {
//             for (var i = 0, nMethods = parseInt(args[3]); i < nMethods; i++) {
//                 /*
//                   https://android.googlesource.com/platform/libnativehelper/+/master/include_jni/jni.h#129
//                   typedef struct {
//                      const char* name;
//                      const char* signature;
//                      void* fnPtr;
//                   } JNINativeMethod;
//                 */
//                 var structSize = pSize * 3; // = sizeof(JNINativeMethod)
//                 var methodsPtr = ptr(args[2]);
//                 var signature = methodsPtr.add(i * structSize + pSize).readPointer();
//                 var fnPtr = methodsPtr.add(i * structSize + (pSize * 2)).readPointer(); // void* fnPtr
//                 var jClass = jclassAddress2NameMap[args[0]].split('/');
//                 console.log('\x1b[3' + '6;01' + 'm', JSON.stringify({
//                     module: DebugSymbol.fromAddress(fnPtr)['moduleName'], // https://www.frida.re/docs/javascript-api/#debugsymbol
//                     package: jClass.slice(0, -1).join('.'),
//                     class: jClass[jClass.length - 1],
//                     method: methodsPtr.readPointer().readCString(), // char* name
//                     signature: signature.readCString(), // char* signature TODO Java bytecode signature parser { Z: 'boolean', B: 'byte', C: 'char', S: 'short', I: 'int', J: 'long', F: 'float', D: 'double', L: 'fully-qualified-class;', '[': 'array' } https://github.com/skylot/jadx/blob/master/jadx-core/src/main/java/jadx/core/dex/nodes/parser/SignatureParser.java
//                     address: fnPtr
//                 }), '\x1b[39;49;00m');
//             }
//         }
//     });
// };

Java.perform(RevealNativeMethods);

function md5Called(args) {
    console.log("MD5 onEnter begin...");
    var backtrace = Thread.backtrace(this.context, Backtracer.ACCURATE).map(DebugSymbol.fromAddress).join("\n\t");
    console.log("Backtrace:" + backtrace);
    console.log("MD5 onEnter end...");
}
Interceptor.attach(Module.findExportByName(null, "MD5_Final"), {
    onEnter: function (args) {
        if (logTimeCall) {
            console.log("MD5_Final onEnter begin...");
            var backtrace = Thread.backtrace(this.context, Backtracer.ACCURATE).map(DebugSymbol.fromAddress).join("\n\t");
            console.log("Backtrace:" + backtrace);
            console.log("MD5_Final onEnter end...");
        }
    },
});
Interceptor.attach(Module.findExportByName(null, "MD5_Update"), {
    onEnter: function (args) {
        if (logTimeCall) {
            console.log("MD5_Update onEnter begin...");
            console.log("MD5_Update " + ' src:' + Memory.readCString(ptr(args[1])));
            var backtrace = Thread.backtrace(this.context, Backtracer.FUZZY).map(DebugSymbol.fromAddress).join("\n\t");
            console.log("Backtrace:" + backtrace);
            console.log("MD5_Update onEnter end...");
        }
    },
});
Interceptor.attach(Module.findExportByName(null, "EVP_DigestUpdate"), {
    onEnter: function (args) {
        if (logTimeCall) {
            console.log("EVP_DigestUpdate onEnter begin...");
            console.log("EVP_DigestUpdate " + ' src:' + Memory.readCString(ptr(args[1])));
            var backtrace = Thread.backtrace(this.context, Backtracer.ACCURATE).map(DebugSymbol.fromAddress).join("\n\t");
            console.log("Backtrace:" + backtrace);
            console.log("EVP_DigestUpdate onEnter end...");
        }
    },
});
Interceptor.attach(Module.findExportByName(null, "MD5"), {
    onEnter: function (args) {
        if (logTimeCall) {
            console.log("MD5 onEnter begin...");
            var backtrace = Thread.backtrace(this.context, Backtracer.ACCURATE).map(DebugSymbol.fromAddress).join("\n\t");
            console.log("Backtrace:" + backtrace);
            console.log("MD5 onEnter end...");
        }
    },
});
Interceptor.attach(Module.findExportByName(null, "md5"), {
    onEnter: function (args) {
        if (logTimeCall) {
            console.log("md5 onEnter begin...");
            var backtrace = Thread.backtrace(this.context, Backtracer.ACCURATE).map(DebugSymbol.fromAddress).join("\n\t");
            console.log("Backtrace:" + backtrace);
            console.log("md5 onEnter end...");
        }
    },
});
Interceptor.attach(Module.findExportByName(null, "Md5"), {
    onEnter: function (args) {
        if (logTimeCall) {
            console.log("Md5 onEnter begin...");
            var backtrace = Thread.backtrace(this.context, Backtracer.ACCURATE).map(DebugSymbol.fromAddress).join("\n\t");
            console.log("Backtrace:" + backtrace);
            console.log("Md5 onEnter end...");
        }
    },
});

var encryptorLib = 'libmgencryptor.so', excludeList = ['Alot', 'Of', 'UI', 'Related', 'Functions'];
Module.enumerateRanges(encryptorLib, 'rw')
    .forEach(function (memory) {
        // console.log('' + encryptorLib + ' base:' + memory.base + ' size:' + memory.size + ' protection:' + memory.protection);
    });

// Module.enumerateExports(encryptorLib)
//   .filter(function(e) {
//     var fromTypeFunction = e.type == 'function';
//     var notInExcludes = excludeList.indexOf(e.name) == -1;
//     return fromTypeFunction && notInExcludes;
//   })
//   .forEach(function(e) {
//     Interceptor.attach(Module.findExportByName(encryptorLib, e.name), {
//       onEnter: function(args) {
//         console.log(encryptorLib + "#'" + e.name + "'");
//       }
//     });
// });
var libc = Module.findBaseAddress('libmgencryptor.so');
console.log(hexdump(libc, {
  offset: 0,
  length: 64,
  header: true,
  ansi: true
}));
