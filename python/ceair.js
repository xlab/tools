console.log("Script loaded successfully!");

var TEST_ACTIVITY = true;
var TEST_AES = true;
var TEST_BASE64 = false;
var TEST_DES = true;
var TEST_COMPONENT = true;
var TEST_EBI_BASE64 = true;
var TEST_HMAC = true;
var TEST_RSA = true;
var TEST_HSMCLI = true;

if (TEST_ACTIVITY) {
// test code ensure hook works.
    Java.perform(function x() {
        var Activity = Java.use("android.app.Activity");
        Activity.onResume.implementation = function () {
            console.log("onResume:" + this);
            this.onResume();
        };
    });
}

if (TEST_AES) {
    Java.perform(function x() {
        var AESCipher = Java.use("com.rytong.tools.crypto.AESCipher");
        AESCipher.decrypt.overload('java.lang.String', '[B', '[B').implementation = function (arg1, arg2, arg3) {
            // Java.perform(function() {
            //     console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            // });
            console.log('AESCipher.decrypt ' + arg1 + ' ' + arg2);
            result = this.decrypt(arg1, arg2, arg3);
            console.log('AESCipher.decrypt result:' + result);
            return result;
        };
        AESCipher.decrypt.overload('[B', '[B', '[B').implementation = function (arg1, arg2, arg3) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('AESCipher.decrypt 1 ');
            // console.log('AESCipher.decrypt byte ' + arg1 + ' ' + arg2);
            result = this.decrypt(arg1, arg2, arg3);
            // console.log('AESCipher.decrypt byte  result:' + result);
            return result;
        };
        AESCipher.decrypt_uzip.overload('java.lang.String', '[B', '[B').implementation = function (arg1, arg2, arg3) {
            // Java.perform(function() {
            //     console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()))
            // });
            console.log('AESCipher.decrypt_uzip ' + arg1);
            var result = this.decrypt_uzip(arg1, arg2, arg3);
            console.log('AESCipher.decrypt_uzip:' + result);
            return result;
        };
        AESCipher.encrypt.overload('java.lang.String', '[B', '[B', "android.content.Context").implementation = function (arg1, arg2, arg3) {
            // Java.perform(function() {
            //     console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            // });
            console.log('AESCipher.decrypt ' + arg1);
            result = this.encrypt(arg1, arg2);
            console.log('AESCipher.decrypt result:' + result);
            return result;
        };
        AESCipher.encrypt.overload('[B', '[B', '[B').implementation = function (arg1, arg2, arg3) {
            // Java.perform(function() {
            //     console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            // });
            console.log('AESCipher.decrypt byte ');
            result = this.encrypt(arg1, arg2, arg3);
            // console.log('AESCipher.decrypt byte  result:' + result);
            return result;
        };
    });
}

if (TEST_COMPONENT) {
    Java.perform(function x() {
        var Component = Java.use("com.rytong.tools.ui.Component");
        Component.encryptValue.overload('java.lang.String', 'java.lang.String', 'com.rytong.tools.httpconnect.WaitDialog$Task').implementation = function (arg1, arg2, arg3) {
            // Java.perform(function() {
            //     console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            // });
            console.log('Component.encryptValue ' + arg1 + ' ' + arg2);
            result = this.encryptValue(arg1, arg2, arg3);
            console.log('Component.encryptValue result:' + result);
            return result;
        };
    });
}

if (TEST_BASE64) {
    Java.perform(function x() {
        var Base64_tools = Java.use("com.rytong.tools.crypto.Base64_tools");
        Base64_tools.decodeToBytes.overload('java.lang.String').implementation = function (arg1) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('Base64_tools.decodeToString ' + arg1);
            result = this.decodeToBytes(arg1);
            // console.log('Base64_tools.decodeToString result:' + result);
            return result;
        };
        Base64_tools.decodeToString.overload('java.lang.String').implementation = function (arg1) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('Base64_tools.decodeToString ' + arg1);
            result = this.decodeToString(arg1);
            console.log('Base64_tools.decodeToString result:' + result);
            return result;
        };
        Base64_tools.encode.overload('[B', "int", "int", "java.lang.StringBuffer").implementation = function (arg1, arg2, arg3, arg4) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('Base64_tools.encode');
            result = this.encode(arg1, arg2, arg3, arg4);
            console.log('Base64_tools.encode result:' + result.toString());
            return result;
        };
    });
}

if (TEST_DES) {
    Java.perform(function x() {
        var DESCipher = Java.use("com.rytong.tools.crypto.DESCipher");
        DESCipher.doDecrypt.overload('java.lang.String', "[B").implementation = function (arg1, arg2) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('DESCipher.doDecrypt ' + arg1);
            result = this.doDecrypt(arg1, arg2);
            console.log('DESCipher.doDecrypt result:' + result);
            return result;
        };
        DESCipher.doDecryptWithoutPadding.overload('java.lang.String',"[B").implementation = function (arg1, arg2) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('DESCipher.doDecryptWithoutPadding ' + arg1);
            result = this.doDecryptWithoutPadding(arg1, arg2);
            // console.log('DESCipher.doDecryptWithoutPadding result:' + result);
            return result;
        };
        DESCipher.doEncrypt.overload('java.lang.String',"[B").implementation = function (arg1, arg2) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('DESCipher.doEncrypt ' + arg1);
            result = this.doEncrypt(arg1, arg2);
            console.log('DESCipher.doEncrypt result:' + result.toString());
            return result;
        };
    });
}

if (TEST_EBI_BASE64) {
    Java.perform(function x() {
        var EBIBase64 = Java.use("com.rytong.tools.crypto.EBIBase64");
        EBIBase64.decode.overload('java.lang.String').implementation = function (arg1) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('EBIBase64.decode ' + arg1);
            result = this.decode(arg1);
            // console.log('EBIBase64.decode result:' + result);
            return result;
        };
        EBIBase64.encode.overload('[B').implementation = function (arg1) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('EBIBase64.encode');
            result = this.encode(arg1);
            console.log('EBIBase64.encode result:' + result);
            return result;
        };
        EBIBase64.getBase64Code.overload("java.lang.String").implementation = function (arg1) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('EBIBase64.getBase64Code ' + arg1);
            result = this.getBase64Code(arg1);
            console.log('EBIBase64.getBase64Code result:' + result.toString());
            return result;
        };
    });
}

if (TEST_EBI_BASE64) {
    Java.perform(function x() {
        var HMac = Java.use("com.rytong.tools.crypto.HMac");
        HMac.MD5.overload('[B').implementation = function (arg1) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('HMac.MD5');
            result = this.MD5(arg1);
            // console.log('HMac.MD5 result:' + result);
            return result;
        };
        HMac.PRF.overload('[B', '[B', '[B', 'int').implementation = function (arg1, arg2, arg3, arg4) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('HMac.PRF');
            result = this.PRF(arg1, arg2, arg3, arg4);
            // console.log('HMac.PRF result:' + result);
            return result;
        };
        HMac.SHA1.overload("[B").implementation = function (arg1) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('HMac.SHA1');
            result = this.SHA1(arg1);
            // console.log('HMac.SHA1 result:' + result.toString());
            return result;
        };
        HMac.encryptHMAC.overload('[B', '[B', "java.lang.String").implementation = function (arg1, arg2, arg3) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('HMac.encryptHMAC ' + arg3);
            result = this.encryptHMAC(arg1, arg2, arg3);
            // console.log('HMac.encryptHMAC result:' + result.toString());
            return result;
        };
    });
}

if (TEST_RSA) {
    Java.perform(function x() {
        var RSACipher = Java.use("com.rytong.tools.crypto.RSACipher");
        RSACipher.doEncrypt.implementation = function (arg1, arg2,  arg3) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('RSACipher.doEncrypt ' + arg3);
            result = this.doEncrypt(arg1, arg2, arg3);
            // console.log('RSACipher.doEncrypt result:' + result);
            return result;
        };
        RSACipher.doEncryptWithBase64.implementation = function (arg1, arg2, arg3) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('RSACipher.doEncryptWithBase64 ' + arg3);
            result = this.doEncryptWithBase64(arg1, arg2, arg3);
            // console.log('RSACipher.doEncryptWithBase64 result:' + result);
            return result;
        };
        RSACipher.doEncryptWithCertUserArr.implementation = function (arg1, arg2) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('RSACipher.doEncryptWithCertUserArr ' + arg2);
            result = this.doEncryptWithCertUserArr(arg1, arg2);
            console.log('RSACipher.doEncryptWithCertUserArr result:' + result.toString());
            return result;
        };
        RSACipher.encrypt.overload('java.security.Key', '[B').implementation = function (arg1, arg2) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('RSACipher.encrypt');
            result = this.encrypt(arg1, arg2);
            // console.log('RSACipher.encrypt result:' + result.toString());
            return result;
        };
    });
}

if (TEST_HSMCLI) {
    Java.perform(function x() {
        var hsmcli = Java.use("com.rytong.tools.crypto.hsmcli");
        hsmcli.PkEncryptAPin.implementation = function (arg1) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('hsmcli.PkEncryptAPin ' + arg1);
            result = this.PkEncryptAPin(arg1);
            console.log('hsmcli.PkEncryptAPin result:' + result);
            return result;
        };
        hsmcli.PkEncryptEPin.implementation = function (arg1) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('hsmcli.PkEncryptEPin ' + arg1);
            result = this.PkEncryptEPin(arg1);
            console.log('hsmcli.PkEncryptEPin result:' + result);
            return result;
        };
    });
}

if (true) {
    Java.perform(function x() {
        var ClientHello = Java.use("com.rytong.tools.clienthello.ClientHello");
        ClientHello.encode.implementation = function (arg1, arg2) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('ClientHello.encode ' + arg1, + ' ' + arg2);
            result = this.PkEncryptAPin(arg1, arg2);
            console.log('ClientHello.encode result:' + result);
            return result;
        };
        ClientHello.getAESIv.implementation = function (arg1) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('ClientHello.getAESIv');
            result = this.getAESIv(arg1);
            // console.log('ClientHello.getAESIv result:' + result);
            return result;
        };
        ClientHello.getAESKey.implementation = function (arg1) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('ClientHello.getAESKey');
            result = this.getAESKey(arg1);
            // console.log('ClientHello.getAESKey result:' + result);
            return result;
        };
        ClientHello.getHMacKey.implementation = function (arg1) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            console.log('ClientHello.getHMacKey');
            result = this.getHMacKey(arg1);
            // console.log('ClientHello.getHMacKey result:' + result);
            return result;
        };
    });
}