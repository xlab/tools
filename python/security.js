Java.perform(function () {

    var Security = Java.use("com.peersless.security.Security");

    Security.InitModule.implementation = function (c, i) {

        send("InitModule");
        send(c.toString());
        send(i.toString());
        this.InitModule(c, i);
    };
    Security.nativeInit.implementation = function (c, i) {

        send("nativeInit");
        send(c.toString());
        send(i.toString());
        this.nativeInit(c, i);
    };
});
