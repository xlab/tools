console.log("Script loaded successfully 55");

TEST_TT = true;
TEST_MGENCRYPT = false;
TEST_MD5CRYPT = false;

if (TEST_TT) {
    Java.perform(function x() {
        var tt = Java.use("com.ss.sys.ces.gg.tt$1");
        tt.a.overload('java.lang.String', 'java.util.Map').implementation = function (arg1, arg2) {
            Java.perform(function() {
                console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
            });
            if (arg2 != null) {
                var iter = arg2.keySet().iterator();
                while(iter.hasNext()) {
                    var key = iter.next();
                    var value = arg2.get(key);
                    console.log("key: " + key + " value type: " + typeof value + " value: " + value);
                    // if (value != null) {
                    //     console.log("key: " + key + " item0: " + value.get(0));
                    // }
                }
            }
            console.log('a: ' + arg1, + ' ' + arg2);
            var ret = this.a(arg1, arg2);
            console.log('a ret: ' + ret);
            return ret;
        };
    });
}

if (TEST_MGENCRYPT) {
    Java.perform(function x() {
        var MGEncryptor = Java.use("com.cmcc.migutv.encryptor.MGEncryptor");
        MGEncryptor.getSignFromNative.implementation = function (arg1, arg2) {
            // Java.perform(function() {
            //     console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()))
            // });
            console.log('getSignFromNative: ' + arg2);
            var ret = this.getSignFromNative(arg1, arg2);
            console.log('getSignFromNative ret: ' + ret);
            return ret;
        };
        MGEncryptor.getMiGuSign.implementation = function (arg1, arg2) {
            // Java.perform(function() {
            //     console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()))
            // });
            console.log('getMiGuSign: ' + arg2);
            var ret = this.getSignFromNative(arg1, arg2);
            console.log('getMiGuSign ret: ' + ret);
            return ret;
        }

    });  
}

if (TEST_MD5CRYPT) {
    Java.perform(function x() {
        var Md5Crypt = Java.use("org.apaches.commons.codec.digest.Md5Crypt");
        Md5Crypt.md5Crypt.overload('[B', 'java.lang.String', 'java.lang.String').implementation = function (arg1, arg2, arg3) {
            console.log('md5Crypt salt:' + arg2 + ' prefix:' + arg3);
            var ret = this.md5Crypt(arg1, arg2, arg3);
            console.log('md5Crypt ret: ' + ret);
            return ret;
        };
        Md5Crypt.apr1Crypt.overload('[B', 'java.lang.String').implementation = function (arg1, arg2) {
            console.log('apr1Crypt ' + arg2);
            var ret = this.apr1Crypt(arg1, arg2);
            console.log('apr1Crypt ret: ' + ret);
            return ret;
        };
    });
    
}