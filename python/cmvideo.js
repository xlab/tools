console.log("Script loaded successfully!");

var TEST_DES = false;
var TEST_MGENCRYPT = true;
var TEST_MD5CRYPT = true;
var TEST_RUNTIME = true;
var VERSION_5_6_8_30 = false;
var VERSION_5_3_0 = true;
var targetMd5 = false;


function getHexString(data) {
    console.log("getHexString data length: " + data.size());
    var hexstr = '';
    for (var i = 0; i < data.length; i++) {
        b = (data[i] >>> 0) & 0xff;
        n = b.toString(16);
        hexstr += ("00" + n).slice(-2) + " ";
        console.log('getHexString: ' + i);
    }
    return hexstr;
}

if (VERSION_5_6_8_30) {
    console.log("version 5.6.8.30 TEST_DES: " + TEST_DES + " TEST_DES: " + TEST_MGENCRYPT + " TEST_MD5CRYPT: " + TEST_MD5CRYPT);
    if (TEST_DES) {
        Java.perform(function x() {
            var DESUtil = Java.use("com.hmt.analytics.common.DESUtil");
            DESUtil.encode.implementation = function (s1, s2) {
                send('encode: ' + s1, + ' ' + s2);
                return this.parseVideoWithParamsForDL(s1, s2);
            };
            DESUtil.decode.implementation = function (s1, s2) {
                send('decode: ' + s1, + ' ' + s2);
                return this.decode(s1, s2);
            };
        });
    }

    if (TEST_MGENCRYPT) {
        Java.perform(function x() {
            var MGEncryptor = Java.use("com.cmcc.migutv.encryptor.MGEncryptor");
            MGEncryptor.getSignFromNative.implementation = function (arg1, arg2) {
                // Java.perform(function() {
                //     console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()))
                // });
                console.log('getSignFromNative: ' + arg2);
                var ret = this.getSignFromNative(arg1, arg2);
                console.log('getSignFromNative ret: ' + ret);
                return ret;
            };
            MGEncryptor.getMiGuSign.implementation = function (arg1, arg2) {
                // Java.perform(function() {
                //     console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()))
                // });
                console.log('getMiGuSign: ' + arg2);
                var ret = this.getSignFromNative(arg1, arg2);
                console.log('getMiGuSign ret: ' + ret);
                return ret;
            }

        });
    }

    if (TEST_MD5CRYPT) {
        Java.perform(function x() {
            var Md5Crypt = Java.use("org.apaches.commons.codec.digest.Md5Crypt");
            Md5Crypt.md5Crypt.overload('[B', 'java.lang.String', 'java.lang.String').implementation = function (arg1, arg2, arg3) {
                console.log('md5Crypt salt:' + arg2 + ' prefix:' + arg3);
                var ret = this.md5Crypt(arg1, arg2, arg3);
                console.log('md5Crypt ret: ' + ret);
                return ret;
            };
            Md5Crypt.apr1Crypt.overload('[B', 'java.lang.String').implementation = function (arg1, arg2) {
                console.log('apr1Crypt ' + arg2);
                var ret = this.apr1Crypt(arg1, arg2);
                console.log('apr1Crypt ret: ' + ret);
                return ret;
            };
        });

    }
} else if (VERSION_5_3_0) {
    console.log("version 5.3.0 TEST_DES: " + TEST_DES + " TEST_DES: " + TEST_MGENCRYPT + " TEST_MD5CRYPT: " + TEST_MD5CRYPT);
    if (TEST_MGENCRYPT) {
        Java.perform(function x() {
            var SecurityEnvSDK = Java.use("com.wireless.security.securityenv.sdk.SecurityEnvSDK");
            SecurityEnvSDK.getToken.implementation = function () {
                // Java.perform(function() {
                //     console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()))
                // });
                var ret = this.getToken();
                console.log('getToken ret: ' + ret);
                return ret;
            };
        });
        Java.perform(function x() {
            var MGEncryptor = Java.use("com.cmcc.migutv.encryptor.MGEncryptor");
            MGEncryptor.getSignFromNative.implementation = function (arg1, arg2) {
                // Java.perform(function() {
                //     console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()))
                // });
                console.log('getSignFromNative: ' + arg2);
                var ret = this.getSignFromNative(arg1, arg2);
                console.log('getSignFromNative ret: ' + ret);
                return ret;
            };
            MGEncryptor.getMiGuSign.implementation = function (arg1, arg2) {
                // Java.perform(function() {
                //     console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()))
                // });
                targetMd5 = true;
                console.log('getMiGuSign: ' + arg2);
                var ret = this.getMiGuSign(arg1, arg2);
                console.log('getMiGuSign ret: ' + ret);
                targetMd5 = false;
                return ret;
            };
            var classLoader = MGEncryptor.$new().getClass().getClassLoader();
            var BaseDexClassLoader = Java.use("dalvik.system.BaseDexClassLoader");
            var baseDexClassLoader = Java.cast(classLoader, BaseDexClassLoader);
            console.log("MGEncrypto classLoader: " + baseDexClassLoader.pathList.value);
        });
    }

    if (TEST_MD5CRYPT) {
        Java.perform(function x() {
            var MessageDigest = Java.use("java.security.MessageDigest");
            var String = Java.use("java.lang.String");
            var ByteString = Java.use("com.android.okhttp.okio.ByteString");
            // var System = Java.use("java.lang.System");
            // System.currentTimeMillis.implementation = function () {
            //     Java.perform(function() {
            //         console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()))
            //     });
            //     return this.currentTimeMillis();
            // };
            MessageDigest.update.overload("[B").implementation = function (arg1) {
                if (true || targetMd5) {
                    // Java.perform(function() {
                    //     console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()))
                    // });
                    console.log("MessageDigest.update: " + String.$new(arg1));
                }
                return this.update(arg1);
            };
            // MessageDigest.update.overload("[B", "int", "int").implementation = function (arg1, arg2, arg3) {
            //     // Java.perform(function() {
            //     //     console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()))
            //     // });

            //     var ret = this.update(arg1, arg2, arg3);
            //     if (arg3 != 20 && (true || targetMd5)) {
            //         console.log("MessageDigest.update(byte[] buf, int offset, int len): " + String.$new(arg1) + ' offset: ' + arg2 + ' len: ' + arg3);
            //     }
            //     return ret;
            // };
            MessageDigest.digest.overload().implementation = function () {
                var ret = this.digest();
                if (true || targetMd5) {
                    console.log("MessageDigest.digest(): " + ByteString.of(ret).hex());
                }
                return ret;
            };

            // MessageDigest.digest.overload("[B", "int", "int").implementation = function (arg1, arg2, arg3) {
            //     // Java.perform(function() {
            //     //     console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()))
            //     // });

            //     var ret = this.digest(arg1, arg2, arg3);
            //     if (arg3 != 20 && (true || targetMd5)) {
            //         console.log("MessageDigest.digest(byte[] buf, int offset, int len): " + ByteString.of(arg1).hex() + ' offset: ' + arg2 + ' len: ' + arg3);
            //     }
            //     return ret;
            // };
            var Md5Crypt = Java.use("org.apaches.commons.codec.digest.Md5Crypt");
            Md5Crypt.md5Crypt.overload('[B', 'java.lang.String', 'java.lang.String').implementation = function (arg1, arg2, arg3) {
                console.log('md5Crypt salt:' + arg2 + ' prefix:' + arg3);
                var ret = this.md5Crypt(arg1, arg2, arg3);
                console.log('md5Crypt ret: ' + ret);
                return ret;
            };
            Md5Crypt.apr1Crypt.overload('[B', 'java.lang.String').implementation = function (arg1, arg2) {
                console.log('apr1Crypt ' + arg2);
                var ret = this.apr1Crypt(arg1, arg2);
                console.log('apr1Crypt ret: ' + ret);
                return ret;
            };
            var NetworkUitlity = Java.use("com.hmt.analytics.common.NetworkUitlity");
            var String = Java.use("java.lang.String");
            console.log("salt: " + JSON.stringify(NetworkUitlity.SAULT));
            console.log("salt: " + NetworkUitlity.SAULT.value);
            // var text = Java.cast(NetworkUitlity.SAULT, String);
            console.log("salt: " + String.$new(NetworkUitlity.SAULT.toString()));
            var VideoDetailsObject = Java.use("com.cmcc.cmvideo.foundation.player.model.VideoDetailsObject");
            VideoDetailsObject.getOriginVideoInfo.overload('java.lang.String', 'java.lang.String', 'boolean', 'boolean', 'boolean').implementation = function (arg1, arg2, arg3, arg4, arg5) {
                console.log('VideoDetailsObject getOriginVideoInfo arg1:' + arg1 + ' arg2:' + arg2);
                this.getOriginVideoInfo(arg1, arg2, arg3, arg4, arg5);
            };    
            var B64 = Java.use("org.apache.commons.codec.digest.B64");
            B64.getRandomSalt.implementation = function (arg1) {
                var ret = this.getRandomSalt(arg1);
                console.log('B64.getRandomSalt arg1:' + arg1 + ' ret:' + ret);
                return ret;
            };    
            var Crypt = Java.use("org.apache.commons.codec.digest.Crypt");
            Crypt.crypt.overload("[B", "java.lang.String").implementation = function (arg1, arg2) {
                var ret = this.crypt(arg1, arg2);
                console.log('Crypt.crypt arg1:' + String.$new(arg1) + ' arg2:' + arg2 + ' ret:' + ret);
                return ret;
            };    
            var Sha2Crypt = Java.use("org.apache.commons.codec.digest.Sha2Crypt");
            Sha2Crypt.sha256Crypt.overload("[B", "java.lang.String").implementation = function (arg1, arg2) {
                var ret = this.sha256Crypt(arg1, arg2);
                console.log('Sha2Crypt.sha256Crypt arg1:' + String.$new(arg1) + ' arg2:' + arg2 + ' ret:' + ret);
                return ret;
            };    
        });
    }
    if (TEST_RUNTIME) {
        Java.perform(function x() {
            var Runtime = Java.use("java.lang.Runtime");
            var runtime = Runtime.getRuntime();
            console.log("Runtime info: " + JSON.stringify(runtime));
            console.log("Runtime mLibPaths: " + runtime.mLibPaths.value);
        });        
    }
}