from __future__ import print_function
import frida
import sys
from subprocess import Popen

Popen(["adb forward tcp:27042 tcp:27042"], shell=True).wait()
process = frida.get_device_manager().enumerate_devices()[-1].attach("com.helios.kids")
script = process.create_script("""
Interceptor.attach(ptr("%s"), {
    onEnter: function(args) {
        send(args[0].toInt32());
    }
});
""" % int(sys.argv[1], 16))
def on_message(message, data):
    print(message)
script.on('message', on_message)
script.load()
sys.stdin.read()
