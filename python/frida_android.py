# Written by rotlogix & lavalamp
#
#
import frida
import sys
from subprocess import Popen

# process_name = 'com.tencent.ktsdk.demo'
# process_name = 'com.helios.kids'
# process_name = 'com.youku.phone'
# process_name = 'com.zhongduomei.rrmj.society'
process_name = 'com.cmcc.cmvideo'
# process_name = 'com.ss.android.ugc.aweme'
# process_name = 'com.dzwh.ttys'
# process_name = 'com.cctv4g.cctvmobiletv'
# process_name = 'com.moretv.android'
# process_name = 'com.example.antileech'
# process_name = 'com.TianjinAirlines.androidApp'
# process_name = 'com.tongcheng.android'
# process_name = 'com.banma'
# process_name = 'com.rytong.ceair'
process_id = 9558
# process_name = 'com.blackfish.app.ui'
# target_js_file = 'cipher.js'
# target_js_file = 'youku.js'
# target_js_file = 'loadedClasses.js'
# target_js_file = 'ttys.js'
# target_js_file = 'cctv4g.js'
# target_js_file = 'rrmj.js'
target_js_file = 'cmvideo.js'
# target_js_file = 'douyin.js'
# target_js_file = 'qq.js'
# target_js_file = 'security.js'
# target_js_file = 'tianjinair.js'
# target_js_file = 'blackfish.js'
# target_js_file = 'tongcheng.js'
# target_js_file = 'banma.js'
# target_js_file = 'hbyt.js'
# target_js_file = 'ceair.js'
# due to frida bug, cannot using spwan and resume in python script,
# but we could spwan in command line and run script
SPWAN = False

def on_message(message, data):
    try:
        if message:
            print("[*] {0}".format(message["payload"]))
    except Exception as e:
        print(message)
        print(e)

def get_script():
    script = ''
    with open(target_js_file, 'r') as f:
        script = f.read()

    return script

if __name__ == "__main__":
    try:
        Popen(["adb forward tcp:27042 tcp:27042"], shell=True).wait()
        device = frida.get_device_manager().enumerate_devices()[-1]
        session = None
        if SPWAN:
            process_id = device.spawn([process_name])
            print('process_id:', process_id)
            session = device.attach(process_id)
        else:
            #  process = device.attach(process_name)
            session = device.attach(process_id)
        script = session.create_script(get_script())
        script.on('message', on_message)
        script.load()
        if SPWAN:
            print('resume:', process_id)
            device.resume(process_id)
        sys.stdin.read()
    except KeyboardInterrupt as e:
        sys.exit(0)
