import re
import json
import urllib2

api_fmt = 'http://disp.titan.mgtv.com/vod.do?fmt=4&pno={pno}&fid=1A58A0C71B857913AD1E7B24AC2D2ACA&file=/c1/2018/03/05_0/1A58A0C71B857913AD1E7B24AC2D2ACA_20180305_1_1_950.mp4'
if __name__ == '__main__':

    # print failed_sids
    for pno in xrange(0, 5000):
        api = api_fmt.format(pno=pno)
        headers = {'Range': 'bytes=0-1000'}
        request = urllib2.Request(api, headers = headers)
        try:
            print pno,
            response = urllib2.urlopen(request)
            js = json.loads(response.read())
            print(js['status'])
        except Exception, e:
            print e
        else:
            pass
