from operator import xor

pkg_code = 0xc3
member_aes_code = 0xb5
xor_code = member_aes_code
# origin_text = 'com.moretv.kids:accService'
# moretv member
# origin_text = 'MfkEQ^j9TJ$2*#Drx1!v7k4FWGVq3*k&'
# helios member
# origin_text = 'FucAe^j9TJ$2*#Drx1!vb84ZXGVq3*j&'
# kidsedu member
origin_text = 'KidAe^j9TJ$2*#Drx1!vb84ZXGVq3*b&'

def get_confused(plain):
    confused = []
    for c in plain:
        confused.append(xor(ord(c), xor_code))

    return confused

def get_plain(confused):
    plain = ''
    for i in confused:
        plain += chr(xor(i, xor_code))

    return plain

def main():
    confused_key = get_confused(origin_text)
    print confused_key
    for i in confused_key:
        print '0x%x,' %(i),
    print
    print get_plain(confused_key)

if __name__ == '__main__':
    main()
