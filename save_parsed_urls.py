import re
import json

if __name__ == '__main__':
    file_content = None
    with open('funshion.log','r') as f:
        file_content = f.read()

    host_list = re.findall('funshion_result_mp4_(.*)', file_content)
    host_count = {}
    for url in host_list:
        if url in host_count:
            host_count[url] = host_count[url] + 1
        else:
            host_count[url] = 1

    lines = []
    for key in host_count.keys():
        lines.append({'page': key, 'count': host_count[key]})

    lines.sort(key=lambda k: k['count'], reverse=True)

    with open('funshion.json', 'w') as outfile:
        json.dump(lines, outfile)

