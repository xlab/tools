import re
import json
import urllib2

api_fmt = 'http://106.75.17.223/index.php?r=site/search&name={name}'
if __name__ == '__main__':
    file_content = None
    with open('test(1).txt','r') as f:
        file_content = f.read()

    # print file_content
    # sid_js = json.loads(file_content)
    keywords = file_content.split('\n')
    results = []
    for keyword in keywords:
        try:
            api = api_fmt.format(name=keyword)
            request = urllib2.Request(api)
            response = urllib2.urlopen(request)
            js = json.loads(response.read())
            if js['status'] == 200:
                entry = {}
                entry['name'] = keyword
                entry['path'] = js['path']
                results.append(entry)
                print keyword, js['path']
        except Exception, e:
            print e

    with open('test.json', 'w') as f:
        json.dump(results, f)

