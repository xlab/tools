#!/usr/bin/python
# -*- coding: utf8 -*-

from __future__ import division
from mysqllib import *
import datetime
from json import *
import json
import csv
import codecs
from conf import *
import traceback

from multiprocessing import Process
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
mysql = MysqlLib()

sourceSites = {
    'letv': '乐视轮播高清',
    'migu': '咪咕采集',
    'nbtv': '宁波广电',
    'laifeng': '来疯网',
    'pplive': 'pptvp2p直播',
    'huya': '虎牙直播',
    'sohu': '搜狐',
    'youku': '优酷',
    'bilibili': 'B站',
    'wasulive': '华数',
    'livewasu': '华数高清',
    'kankanews': '看看卫视直播',
    'mgtv': '芒果',
    'cmvideo': '咪咕直播',
    'douyu': '斗鱼直播',
    'cztvlive': '新蓝网直播',
    'zhanqi': '战旗直播',
    'qq': '腾讯直播',
    'news_qq': '腾讯新闻直播',
    'pptv': 'PPTV电视直播',
    'egame': '企鹅直播',
    'hogo': '地方广电直播',
    'iptv': 'iptv',
    'huajiao': '花椒直播',
    'gxtv': '广西电视',
    '163app': '网易app',
    '163pc': '网易pc',
    'cctv': 'cctv体育',
    'tencent2': '腾讯注入2',
    'iqilu': '齐鲁网',
    'ifengsi':'粉丝网',
    'ysten':'易视腾',
    'yy':'yy直播',
    'quanmin':'全民直播',
    'wowtv':'wowtv',
    'dltv':'大连广电',
    'hnntv':'海南广电',
    'jxntv':'江西广电',
     'bili':'猫频道',
    'cntv':'央视网直播',
    'panda':'熊猫直播',
    'ttmz':'无锡广电',
    'pingan':'平安好医生',
    'youtu8': '悠兔视频',
    'yitv':"一直播",
    'cztvios':"cztv",
    'jstv':"江苏广电",
    'voole':"voole"
}

diagramSites = {
    'letvp2p_hd': '乐视p2p',

    'pplive': 'pptvp2p直播',
    'ysten': '易视腾',
    'sohu': '搜狐',
    'youku': '优酷',
    'bilibili': 'B站',
    # 'kankanews': '看看新闻',
    'mgtv': '芒果',
    # 'r2048tv': '咪咕直播',
    'douyu': '斗鱼直播',
    # 'cztv': '新蓝网直播',
    #'zhanqi': '战旗直播',
    'qq': '腾讯直播',
    # 'news_qq': '腾讯新闻直播',
    # 'pptv': 'PPTV电视直播',
    'huajiao': '花椒直播',
    'iptv': 'iptv',
    '163app': '网易直播',
    # 'yizhibo': '一直播',
    # 'iqilu': '齐鲁网',
    # 'sina': '新浪直播',
    # 'yy':'yy直播',
    'quanmin':'全民直播',
    'cntv':'央视网直播',
    'panda':'熊猫直播',
    'cmvideo':'咪咕直播'
}

def getDay(number):
    today = datetime.date.today()
    oneday = datetime.timedelta(days=number)
    yesterday = today - oneday
    day = yesterday.strftime("%Y-%m-%d")
    return day

def getGrade(rate):
    rates = ['极佳', '优良', '可用', '不稳定', '慎用', '不可用']
    comments = ['播放成功率高，可作首页推荐大规模使用', '可以作推荐，但是不是最高优先级',
                '该源较为稳定，可作为备选源', '建议源排序降低', '该源建议放到最后', '建议不要使用，如果是唯一源，慎重使用']
    if rate >= 0.9:
        return rates[0], comments[0]
    elif rate >= 0.8 and rate < 0.95:
        return rates[1], comments[1]
    elif rate >= 0.6 and rate < 0.8:
        return rates[2], comments[2]
    elif rate >= 0.4 and rate < 0.6:
        return rates[3], comments[3]
    elif rate >= 0.2 and rate < 0.4:
        return rates[4], comments[4]
    else:
        return rates[5], comments[5]

def getRate(parse_fail_vv, play_fail_vv, parse_play_success_vv):
    # print parse_fail_vv, play_fail_vv, parse_play_success_vv
    total_vv = parse_fail_vv + play_fail_vv + parse_play_success_vv
    parse_fail_rate = round(parse_fail_vv/total_vv, 5)
    play_fail_rate = round(play_fail_vv/total_vv, 5)
    parse_play_success_rate = round((parse_play_success_vv)/total_vv, 3)
    return play_fail_rate, parse_fail_rate, parse_play_success_rate

# 查询主要源站成功率
def query_main_site_parse(month):
    mysql.open_conn()
    sql_fmt = '''SELECT l.`day`,  l.sourceName, s.sum_vv, sum(vv) as define_play_vv, l.playCode
    FROM live_channel_source_code_day_stat l
    JOIN
    (SELECT `day`,sourceName, sum(vv) AS `sum_vv`
        FROM live_channel_source_code_day_stat WHERE `day` between '2017-%02d-01' AND '2017-%02d-31'
        GROUP BY `day`,sourceName) s
        on l.`day`= s.`day` AND l.sourceName = s.sourceName
        WHERE l.playCode in (-3, -2, 200) and l.`day` between '2017-%02d-01' AND '2017-%02d-31' and s.sum_vv>10000
        GROUP BY sourceName, l.playCode, day
        ORDER BY day DESC;'''
    sql = sql_fmt %(month, month, month, month)
    # print sql
    rows = mysql.select(sql)
    dict_all_data = []
    dict_2d = {}
    site = ""
    for item in rows:
        day = item[0]
        site = item[1]
        # print site
        sitename = sourceSites[site] if site in sourceSites else site
        if dict_2d.has_key(day) == False:
            dict_2d[day] = {}

        if dict_2d[day].has_key(site) == False:
            dict_2d[day][site] = {}

        if not dict_2d[day][site].has_key('play_fail_vv') or dict_2d[day][site]['play_fail_vv'] == 0:
            dict_2d[day][site]['play_fail_vv'] = 0

        if not dict_2d[day][site].has_key('parse_play_success_vv') or dict_2d[day][site]['parse_play_success_vv'] == 0:
            dict_2d[day][site]['parse_play_success_vv'] = 0

        if not dict_2d[day][site].has_key('parse_fail_vv') or dict_2d[day][site]['parse_fail_vv'] == 0:
            dict_2d[day][site]['parse_fail_vv'] = 0

        dict_2d[day][site]['sitename'] = sitename
        dict_2d[day][site]['day'] = day
        sum_vv = item[2]
        dict_2d[day][site]['sum_vv'] = sum_vv
        define_play_vv = item[3]
        play_code = item[4]
        dict_2d[day][site]['play_code'] = play_code
        play_code = int(play_code)
        # print play_code, site, day
        # dict_2d[day][site]['parse_play_success_vv'] = 0
        if play_code == -3:
            # print play_code, site, define_play_vv
            dict_2d[day][site]['play_fail_vv'] = define_play_vv
            # print dict_2d[day][site]['play_fail_vv']
        elif play_code == -2:
            # print play_code, site, define_play_vv
            dict_2d[day][site]['parse_fail_vv'] = define_play_vv
        elif play_code == 200:
            # print play_code, site, define_play_vv
            dict_2d[day][site]['parse_play_success_vv'] = define_play_vv
        else:
            dict_2d[day][site]['parse_play_success_vv']=0



                # dict_2d[site]['parse_fail_rate'] = float(parse_fail_rate)
    day_data_dict = sorted(dict_2d.iteritems(), key=lambda asd:asd[1], reverse = True)
    for daykeyitem in iter(day_data_dict):
        daykey = daykeyitem[0]
        # print daykey
        day_data_2d = dict_2d[daykey]
        list_2d = sorted(day_data_2d.iteritems(), key=lambda d:d[1].get('sum_vv',0), reverse = True)
        for list_2d_item in iter(list_2d):
            dict_data_item = {}
            key = list_2d_item[0]
            # print daykey,key
            # print dict_2d[daykey]
            # print dict_2d[daykey][key]['parse_fail_vv']
            dict_2d[daykey][key]['play_fail_rate'], dict_2d[daykey][key]['parse_fail_rate'], dict_2d[daykey][key]['parse_play_success_rate'] = getRate(dict_2d[daykey][key]['parse_fail_vv'], dict_2d[daykey][key]['play_fail_vv'], dict_2d[daykey][key]['parse_play_success_vv'])
            play_fail_rate = 0
            parse_fail_rate = 0
            site_dict = dict_2d[daykey][key]
            if site_dict and site_dict.has_key('play_fail_rate'):
                play_fail_rate =  site_dict['play_fail_rate']
            if site_dict and site_dict.has_key('parse_fail_rate'):
                parse_fail_rate = site_dict['parse_fail_rate']

            success_rate =  site_dict['parse_play_success_rate']
            sum_vv = site_dict['sum_vv']
            priority, comment = getGrade(success_rate)
            # if dict_all_data.has_key(key) == False:
            #     dict_all_data[key] = {}
            if True or daykey == getDay(1):
                # print site_dict['sitename']
                dict_data_item['sitename'] = site_dict['sitename']
                dict_data_item['sum_vv'] = site_dict['sum_vv']
                dict_data_item['play_fail_rate'] = play_fail_rate
                dict_data_item['parse_fail_rate'] = parse_fail_rate
                dict_data_item['success_rate'] = success_rate
                dict_data_item['priority'] = priority
                dict_data_item['comment'] = comment
                dict_data_item['day'] = daykey
                dict_all_data.append(dict_data_item)
            # elif daykey == getDay(7):
            #     # print dict_all_data[key]['success_rate'], success_rate
            #     if dict_all_data.has_key(key) and dict_all_data[key].has_key('success_rate'):
            #         dict_all_data[key]['tongbi_increase_rate'] = dict_all_data[key]['success_rate']-success_rate
            #         dict_all_data[key]['tongbi_increase_playnum'] = dict_all_data[key]['sum_vv']-sum_vv
            #     else:
            #         dict_all_data[key]['tongbi_increase_rate'] = 0
            #         dict_all_data[key]['tongbi_increase_playnum'] = 0
            # elif daykey == getDay(2):
            #     if dict_all_data.has_key(key) and dict_all_data[key].has_key('success_rate'):
            #         dict_all_data[key]['huanbi_increase_rate'] = dict_all_data[key]['success_rate']-success_rate
            #         dict_all_data[key]['huanbi_increase_playnum'] = dict_all_data[key]['sum_vv']-sum_vv
            #     else:
            #         dict_all_data[key]['huanbi_increase_rate'] = 0
            #         dict_all_data[key]['huanbi_increase_playnum'] = 0


            # if dict_all_data[key].has_key('tongbi_increase_rate') == False:
            #     dict_all_data[key]['tongbi_increase_rate'] = 0
            #     dict_all_data[key]['tongbi_increase_playnum'] = 0



            # if dict_all_data[key].has_key('huanbi_increase_rate') == False:
            #     dict_all_data[key]['huanbi_increase_rate'] = 0
            #     dict_all_data[key]['huanbi_increase_playnum'] = 0
            # dict_all_data[key]['backup'] = '无'
            # dict_all_data[key]['from'] = '自研'

    datas = parse_dict_data(dict_all_data)
    store_data(datas, month)



def parse_dict_data(dict_all_data):
    datas = []
    datas.append('日期, 源站, 播放总数, 播放失败率, 解析失败率, 总成功率')
    for dict_data_item in dict_all_data:
        #print key
        if dict_data_item.has_key('sitename') and dict_data_item.has_key('sum_vv') and dict_data_item.has_key('play_fail_rate'):
            data_str = "%s, %s, %s, %s, %s, %s" % (dict_data_item['day'], dict_data_item['sitename'], dict_data_item['sum_vv'], str(dict_data_item['play_fail_rate']), str(dict_data_item['parse_fail_rate']), str(dict_data_item['success_rate']))
            datas.append(data_str)

    return datas


# 查询主要网站的解析趋势图
def query_main_site_parse_trend():
    sql = '''
    SELECT l.`day`,  l.sourceName, s.sum_vv, sum(l.vv) as fail_vv, 1-round(sum(l.vv)/s.sum_vv, 3)as succes_rate
    FROM live_channel_source_code_day_stat l
    JOIN
    (SELECT `day`,sourceName, sum(vv) AS `sum_vv`
        FROM live_channel_source_code_day_stat WHERE playCode in(-3, -2, 200) and `day` >= DATE_SUB(CURDATE(), INTERVAL 7 day) AND sourceName = '%s'
        GROUP BY `day`,sourceName) s
        on l.`day`= s.`day` AND l.sourceName = s.sourceName
        WHERE l.playCode in (-3, -2) and l.`day` >= DATE_SUB(CURDATE(), INTERVAL 7 day)  and l.sourceName = '%s'
        GROUP BY sourceName, `day`
        ORDER BY l.day ASC;
    '''
    datas = [
    ]
    i =0
    days = ['']
    success_rates = []
    sitename = diagramSites[key]
    run_sql = sql % (str(key), str(key))
    mysql.open_conn()
    rows = mysql.select(run_sql)
    success_rates.append(sitename)
    for item in rows:
        day = item[0]
        if i == 0 :
            days.append(day)
        success_rate = item[4]
        success_rates.append(success_rate)
    if i == 0 :
        datas.append(days)
    datas.append(success_rates)
    i = i+1
    store_data(datas)

# 查询主要网站的解析趋势图
def query_main_site_parse_trend2():
    sql = '''
    SELECT l.`day`,  l.sourceName, s.sum_vv, sum(l.vv) as fail_vv, round(sum(l.vv)/s.sum_vv, 3)as fail_rate
    FROM live_channel_source_code_day_stat l
    JOIN
    (SELECT `day`,sourceName, sum(vv) AS `sum_vv`
        FROM live_channel_source_code_day_stat WHERE playCode in(-3, -2, 200) and `day` = DATE_SUB(CURDATE(), INTERVAL %d day) AND sourceName in (%s)
        GROUP BY `day`,sourceName) s
        on l.`day`= s.`day` AND l.sourceName = s.sourceName
        WHERE l.playCode in (-3, -2) and l.`day` = DATE_SUB(CURDATE(), INTERVAL %d day)  and l.sourceName in (%s)
        GROUP BY sourceName, `day`
        ORDER BY sourceName ASC;
    '''
    datas = []
    i =0
    sourceNames = ['']
    sites = ""
    time_slice = 400
    for key in diagramSites:
        if key:
            sites = sites + "'"+key +"',"
    sites = sites[:-1]
    # print sites
    while (time_slice > 0):
        success_rates = []
        run_sql = sql % (time_slice, sites, time_slice, sites)
        print run_sql
        mysql.open_conn()
        rows = mysql.select(run_sql)
        time_slice = time_slice -1
        j = 0
        for item in rows:
            day = item[0]
            sourceName = diagramSites[item[1]]
            if i == 0 :
                sourceNames.append(sourceName)
            if j == 0 :
                success_rates.append(day)
            j = j+1
            success_rate = 1 - item[4]

            success_rates.append(success_rate)
        if i == 0 :
            datas.append(sourceNames)
        datas.append(success_rates)
        i = i+1
    store_data(datas)

def store_data(datas, month):
    # print zip(*table)
    # csvfile = file('livedata.csv', 'w')
    # writer = csv.writer(csvfile)
    # writer.writerows(datas)
    # csvfile.close()

    csv_filename = 'livedata-2017-%02d.csv' % (month)
    with open(csv_filename, 'w') as csvfile:
        for line in datas:
            csvfile.write(line)
            csvfile.write('\n')

    # with open('/data/webapps/tools/livedata.csv','rb') as csvfile:
    #     reader = csv.DictReader(csvfile)
    #     column = [row for row in reader]
    # print column
    # writer = csv.writer(csvfile)
    # writer.writerows(column)
    # csvfile.close()


if __name__ == '__main__':
    # query_main_site_parse()
    for i in xrange(1, 13):
        try:
            query_main_site_parse(i)
        except Exception, e:
            traceback.print_exc()
            traceback.print_stack()
    # query_main_site_parse_trend2()

